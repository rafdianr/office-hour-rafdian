// Deklarasi variabel (var, let, const)
// var -> bisa diubah nilainya dan dideklarasikan ulang, global scope

// var fruit = "apple"
// fruit = "banana"
var fruit = "orange"

if (true) {
    fruit = "apple"
    // console.log('fruit', fruit)
}

// let -> bisa diubah tapi tidak bisa dideklarasikan ulang dalam scope yang sama, local scope

let book = "harry potter"
book = "one piece"

if (true) {
    let dog = "bulldog"
    let book = "naruto"
    // console.log("book", book) // naruto

    if (true) {
        // console.log('dog', dog) // bulldog
    }

}

book = "fifty shade of gray"
// console.log("book", book) // fifty shade of gray

// const -> tidak bisa diubah dan dideklarasikan ulang, local scope

const food = "Nasi goreng"

if(true) {
    const drink = "coca-cola"
    // console.log(food) // nasi goreng
    if (true) {
        const drink = "water"
        // console.log(drink) // water
    }
    // console.log(drink) //coca-cola
}
// console.log(food) // nasi goreng


// OPERATOR ( + , - , * , / , % , ++ , --)

let a = 4;
let b = a++;

console.log(a, b) // 5,4

let c = 4;
let d = ++c;

console.log(c, d) // 5,5

// TIPE Data ( boolean, string, number, undefined, null, NaN) --- primitive
// truthty = value yang mempunyai nilai dan bukan false, falsy = ga punya nilai atau false (undefined, null, NaN)

let e 

// TIPE Data ( object, array, date) --- non-primitive

const detail = { person: "Meca", age: "22"}
const books = [ "harry potter", "Naruto", "one piece" ]
const currentDate = new Date()

console.log(currentDate)

console.log( 2 + " " + "Hari")
console.log(`${detail.person} namanya`)
