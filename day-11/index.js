/*
FUNCTION

function namaFungsi(paramater1, parameter2, ...parameterN) {
    let variable = x
    perintah
    return parameter1 + parameter2
}

let argumen1 = xx
namaFungsi(argumen1, argumen2, ...argumenN)

 */

//CONTOH 1
function addNum(value1, value2){
    return value1 + value2
}

let a = 2
let b = 3

let result = addNum(a, b)
console.log(result)

/*

ARRAY
let array1 = [ 1, 2, 3, 4, 5]
console.log(array1)
[value]
length
console.log("numbers", numbers)
console.log("array length", numbers.length)

OBJECT
let object1 = { person: "NAMA", age: "24"}
console.log(object1.person)
console.log(object1.age)
*/


function getMaxNumber(value){
    let max = value[0]
    for(let i=0; i<value.length; i++) {
        if(max < value[i]) {
            max = value[i]
        }
    }
    return max   
}

let numbers = [ 7, 3, 8, 9, 12, 5]
console.log(getMaxNumber(numbers))

