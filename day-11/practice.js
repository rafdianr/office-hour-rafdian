/*
Buatlah sebuah fungsi yang menghasilkan jumlah dari nilai tertinggi dan terendah dari sebuah array

input: [ 4, 5, 1, 9, 8]
Output: 10
*/

function sumMinMax(arrayInput){
    let max = arrayInput[0]
    let min = arrayInput[0]

    for(let i = 0; i<arrayInput.length; i++){
        if(max < arrayInput[i]) {
            max = arrayInput[i]
        }
        if(min > arrayInput[i]) {
            min = arrayInput[i]
        }
    }
    return max + min

    // let sortArray = arrayInput.sort()
    // return arrayInput[0] + arrayInput[arrayInput.length-1]
}

let input = [ -4, 5, 3, 9, 8]
let result = sumMinMax(input)
console.log("result:", result)