/*
Buat sebuah fungsi yang akan menghitung score dari dua buah input array.

Input:
A = [ 4, 3, 3]
B = [ 5, 2, 2]
Outputnya = scoreA : scoreB = 2 : 1
*/

function sumScore(arr1, arr2){
    let scoreA = 0
    let scoreB = 0
    for (let i = 0; i < arr1.length; i++) {
        if(arr1[i] > arr2[i]) {
            scoreA += 1
        }
        if(arr1[i] < arr2[i]) {
            scoreB += 1
        }
    }
    return [scoreA, scoreB]
}

let a = [ 4, 3, 3]
let b = [ 5, 2, 3]

console.log(sumScore(a,b)) // 1:1