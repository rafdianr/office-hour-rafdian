# GIT

## Perintah Git

```bash
$ git init //menginisiasi git ke folder
$ git remote //ngecek remote yang aktif
$ git remote -v //ngecek remote yang aktif lebih detail
$ git branch //ngecek branch apa aja yg ada
$ git checkout [nama_branch] //pindah ke nama_branch
$ git checkout -b [nama_branch_baru] //buat nama branch baru
$ git push [nama_remote] [nama_branch] //nge push branch ke remote
$ git pull [nama_remote] [nama_branch] //nge pull branch yg ada di remote repository
$ git clone [nama_repository] //nge clone repo
$ git add . //nge-stage perubahan yg dilakukan
$ git commit -m "meassage" //nge-commit perubahan
$ git status //ngelihat status apa aja yg sedang terjadi
$ git log //nge lihat commit yg sudah dilakukan
$ git remote add [nama_remote] [nama_repo_git]
```
