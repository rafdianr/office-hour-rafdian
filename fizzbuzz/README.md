# FizzBuzz

> Latihan ini ditujukan untuk melatih kemampuan memanipulasi HTML DOM dengan menggunakan javascript murni.

Buatlah sebuah program/fungsi sederhana yang mencetak angka antara 1 sampai 100 dengan mengikuti kondisi di bawah ini:

- Jika angka merupakan kelipatan 3, maka akan mencetak `Fizz`
- Jika angka merupakan kelipatan 5, maka akan mencetak `Buzz`
- Jika angka merupakan kelipatan 3 dan 5, maka akan mencetak `FizzBuzz`

Ketentuan tambahan yang harus dilakukan:

1. Buat form `input` untuk memasukan angka untuk dicetak.
2. Pastikan `input` yang gunakan adalah angka 1 sampai 100.
3. Cetak hasilnya ke dalam beberapa bentuk dan warna:
   - Semua angka berada di dalam lingkaran hitam (untuk _border line_ dan teks)
   - Semua hasil `Fizz` berada di dalam lingkaran biru (untuk _border line_ dan teks) dan teks harus tebal
   - Semua hasil `Buzz` berada di dalam lingkaran merah (untuk _border line_ dan teks) dan teks harus tebal
   - Semua hasil `FizzBuzz` berada di dalam lingkaran dengan latar belakang hijau dan teks harus tebal dengan warna putih
4. Tidak boleh menggunakan _framework_ apapun dan gunakan file yang sudah tersedia.

## Contoh Hasil

Berikut adalah contoh dari hasil latihan ini:

![contoh](./example/example.png)
