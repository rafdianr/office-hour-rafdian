function onGenerate() {
  document.getElementById("number-wrap").innerHTML = ""; // Mengkosongkan element dengan id number-wrap

  // Get input value
  let inputValue = parseInt(document.getElementById("number-input").value);

  if (inputValue === 0 || inputValue > 100) {
    alert("You must input number between 1-100");
    return;
  }

  for (let i = 1; i <= inputValue; i++) {
    let node = document.createElement("DIV"); // Buat tag div kosong <div></div>
    node.classList.add("number"); // Nambah class <div class="number"></div>
    let textnode; // Buat nyimpen text yang mau dimasukin
    if (i % 3 === 0 && i % 5 === 0) {
      node.classList.add("fizz-buzz"); // Nambah class baru lagi <div class="number fizz-buzz"></div>
      textnode = document.createTextNode("FizzBuzz"); // Buat nyediain teks
    } else if (i % 3 === 0) {
      node.classList.add("fizz");
      textnode = document.createTextNode("Fizz");
    } else if (i % 5 === 0) {
      node.classList.add("buzz");
      textnode = document.createTextNode("Buzz");
    } else {
      textnode = document.createTextNode(i);
    }
    node.appendChild(textnode); // <div class="number">textnode</div>
    document.getElementById("number-wrap").appendChild(node); // Masukin node ke target elemen
  }

  document.getElementById("number-input").value = 0; // Reset value ke 0
}

// document.querySelector("button").onclick = function () {
//   console.log("click");
// };
