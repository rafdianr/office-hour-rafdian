/*
Buat sebuah fungsi yang menghitung akumulasi score akhir dari sebuah point
- Untuk point <= 10 mendapatkan 1 point
- Untuk point <= 20 mendapatkan 2 point
- Untuk point <= 30 mendapatkan 3 point
- Untuk point > 30 mendapatkan 5 point

input: 19
output: 28 point

input: 8
output: 8

input: 11
output 12

input: 20
output: 30

input: 21 (1*10 + 2*10 + 3*1)
output: 33
*/

function accPoint(value) {
    let score = 0
    if(value <= 10) score = value * 1
    else if (value <= 20) score = value * 2 - 10
    else if (value <= 30) score = value * 3 - 30
    else score = value * 5 - 90

    return score
}

let point = 21
console.log(accPoint(point))

