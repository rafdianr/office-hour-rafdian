import React from "react";
import "../assets/styles/card.css";

const Card = ({ logo, rating, title, description }) => {
  return (
    <div className="card">
      <div className="card__wrapper">
        <div className="card__header">
          <img src={logo} alt="logo" />
          <div className="card__point">
            <span>{rating}</span>
          </div>
        </div>
        <div className="card__body">
          <h6 className="card__title">{title}</h6>
          <div className="card__description">{description}</div>
        </div>
      </div>
    </div>
  );
};

export default Card;
