import React from "react";
import Homepage from "./pages/Homepage";
import Footer from "./layouts/Footer";

const App = () => {
  return (
    <div>
      <Homepage />
      <Footer />
    </div>
  );
};

export default App;
