import React from "react";
import Location from "./Location";

const index = () => {
  return (
    <div className="page">
      <Location />
    </div>
  );
};

export default index;
