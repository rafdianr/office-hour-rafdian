import React, { useState, useEffect } from "react";
import "../../assets/styles/location.css";
import Card from "../../components/Card";
import TokopediaImg from "../../assets/images/tokped.png";
import BukalapakImg from "../../assets/images/bukalapak.png";
import ShopeeImg from "../../assets/images/shopee.png";

const Location = () => {
  const [photos, setPhotos] = useState([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/albums/1/photos")
      .then((response) => response.json())
      .then((json) => setPhotos(json));
  }, []);

  const data = [
    {
      logo: TokopediaImg,
      rating: 4.6,
      title: "Tokopedia",
      description: "Tokopedia lorem ipsum dolor sit amet.",
    },
    {
      logo: BukalapakImg,
      rating: 4.8,
      title: "Bukalapak",
      description: "Bukalapak lorem ipsum dolor sit amet.",
    },
    {
      logo: ShopeeImg,
      rating: 4.2,
      title: "Shopee",
      description: "Shopee lorem ipsum dolor sit amet.",
    },
    {
      logo: ShopeeImg,
      rating: 4.2,
      title: "Lazada",
      description: "Lazada lorem ipsum dolor sit amet.",
    },
  ];

  return (
    <div className="location">
      <div className="location__wrapper">
        <div className="location__content">
          <div className="location__title">Title</div>
          <div className="location__cards">
            {photos.map((item) => (
              <Card
                key={item.id}
                logo={item.thumbnailUrl}
                rating={5}
                title={item.title}
                description={item.title}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Location;
