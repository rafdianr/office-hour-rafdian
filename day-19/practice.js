/**
 * Buatlah sebuah kode yang akan mencari presentase siswa yang memiliki nilai lebih dari atau sama dengan 78.
 *
 * Output:
 * Presentasi siswa yang lulus adalah 66%
 *
 */

const students = [
  { id: 1, name: "Tony", score: 87 },
  { id: 2, name: "Steve", score: 91 },
  { id: 3, name: "Scott", score: 72 },
  { id: 4, name: "Natasha", score: 66 },
  { id: 5, name: "Bruce", score: 77 },
  { id: 6, name: "Carol", score: 82 },
  { id: 7, name: "Pepper", score: 91 },
  { id: 8, name: "Clint", score: 84 },
  { id: 9, name: "Barton", score: 95 },
  { id: 10, name: "Sylvie", score: 75 },
  { id: 11, name: "Strange", score: 82 },
  { id: 12, name: "Rocket", score: 78 },
];

const isPass =
  (students.filter((student) => student.score >= 78).length / students.length) *
  100;

console.log(isPass);
