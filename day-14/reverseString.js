/**
 * Buat suatu fungsi yang akan membalikkan sebuah string yang diinputkan
 *
 * input: Rafdian Ramadhan
 * output: nahdamaR naidfaR
 *
 * input: Bagus
 * output: Sugab
 */

// function reverseString(str) {
//   var newString = "";
//   for (var i = str.length - 1; i >= 0; i--) {
//     newString += str[i];
//   }
//   return newString;
// }

function reverseString(str) {
  return str.split("").reverse().join("");
}

// str.split => [ "B", "a", "g", "u", "s"]
// str.reverse => [ "s", "u", "g", "a", "B"]
// str.join => sugaB

console.log(reverseString("Bagus"));
