class Movie {
  constructor(title, genre, duration, rating) {
    this.title = title;
    this.genre = genre;
    this.duration = duration;
    this.rating = rating;
  }

  introduction() {
    return `${this.title} is a/an ${this.genre} movie running for ${this.duration} and has ${this.rating} rating on the internet.`;
  }

  /**
   * GETTER
   * untuk menampilkan suatu nilai
   */
  get intro() {
    return this.introduction();
  }

  get details() {
    return `1. Title: ${this.title}\n2. Genre: ${this.genre}\n3. Duration : ${this.duration}\n4. Rating : ${this.rating}\n`;
  }

  /**
   * SETTER
   * untuk mengubah suatu nilai
   */
  set setGenre(newGenre) {
    this.genre = newGenre;
  }

  set setRating(newRating) {
    this.rating = newRating;
  }
}

const movie = new Movie("Luca", "animation", "140min", "8.8");
// console.log(movie);
// console.log(movie.title);
// console.log(movie.introduction());
// console.log(movie.intro);
// console.log(movie.details);
// movie.setGenre = "kids";
// movie.setRating = "9.0";
// console.log(movie.details);

class Series extends Movie {
  constructor(title, genre, duration, rating, season, episode) {
    super(title, genre, duration, rating);
    this.season = season;
    this.episode = episode;
  }

  get intro() {
    return `${this.title} is a/an ${this.genre} series currently running on season ${this.season} episode ${this.episode} for ${this.duration} per episode and has ${this.rating} rating on the internet.`;
  }

  get details() {
    return `${super.details}5. Season: ${this.season}\n6. Episode: ${
      this.episode
    }\n`;
  }

  set setSeason(newSeason) {
    this.season = newSeason;
  }
}

const series = new Series("Loki", "superhero", "40mins", "9.0", 1, 6);
// console.log(series);
// console.log(series.details);
// series.setSeason = "finale";
// console.log(series.details);
