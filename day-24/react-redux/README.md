# State Management

Redux adalah state management untuk mempermudah
pertukaran data antar component.

Sebagai state management, redux dapat mempermudah
pertukaran informasi dari beberapa component di tempat yang
berbeda, karena semua state dikelola di satu tempat yang
disebut SSOT (Single Source of Truth).

Tiga hal dasar yang perlu diketahui:

1. Store
2. Reducer
3. Dispatch/Action

## Store

Tempat penyimpanan state secara global. Store diibaratkan
seperti database untuk frontend.
Pembuatan store menggunakan function createStore(), yang
memerlukan reducer sebagai parameter.

## Reducer

Suatu function yang bertugas untuk mengelola state yang ada di
dalam store. State yang ada di dalam store, hanya dapat dikelola
oleh reducer. Pembuatan reducer memerlukan 2 parameter,
yaitu state dan action.

## Dispatch/Action

Tempat untuk dispatch(melakukan) sebuah action yang
nantinya akan memanggil reducer, agar reducer menjalankan
sebuah action tertentu, sesuai type action-nya.
Membuat sebuah action memerlukan sebuah object yang wajib
memiliki key “type “
