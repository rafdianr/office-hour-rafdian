import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addCounter, subCounter } from "../store/actions/counterAction";
import { getAllUsers } from "../store/actions/user";

const Hook = () => {
  const count = useSelector((state) => state.counterRed.counter);
  const title = useSelector((state) => state.counterRed.judul);
  const status = useSelector((state) => state.counterRed.status);
  const dispatch = useDispatch();

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => response.json())
      .then((item) => dispatch(getAllUsers(item)));
  }, [dispatch]);

  return (
    <div>
      <h1>{title}</h1>
      <h1>{count}</h1>
      <div>
        <button onClick={() => dispatch(addCounter("Ditambah"))}>+</button>
        <button onClick={() => dispatch(subCounter("Dikurang"))}>-</button>
      </div>
      <p>{status}</p>
    </div>
  );
};

export default Hook;
