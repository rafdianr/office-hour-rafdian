import React, { Component } from "react";
import Counter from "../components/Counter";

export default class Home extends Component {
  render() {
    return (
      <div>
        <h1>Redux</h1>
        <Counter />
      </div>
    );
  }
}
