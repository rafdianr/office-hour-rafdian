const initialState = {
  user: [],
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case "GET_USERS":
      return {
        ...state,
        user: action.payload,
      };
    default:
      return state;
  }
};

export default user;
