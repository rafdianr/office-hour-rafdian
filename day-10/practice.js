/*
1. Kalo bilangan nya kelipatan 3, cetak "Hala"
2. Kalo bilangan nya kelipatan 5, cetak "Madrid"
3. Kalo bilangan nya kelipatan 5 dan 3, cetak "Hala Madrid"

contoh
input: 20
output: 1, 2, Hala, 4, Madrid, Hala, 7, 8, Hala, Madrid, 11, Hala, 13, 14, Hala Madrid, dst
*/

let num = 20;

// for( let i = 0; i < num; i++) {
//     if (i%3 === 0 && i%5 === 0) {
//         console.log("Hala Madrid")
//     } else if (i%5 === 0) {
//         console.log("Madrid")
//     } else if (i%3 === 0) {
//         console.log("Hala")
//     } else {
//         console.log(i)
//     }
// }

/* 
input: [ 7, 3, 8, 9, 6, 5 ]
output: 9
Perintah: munculkan nilai array tertinggi
*/

let numbers = [7, 3, 8, 9, 12, 5];

let max = numbers[0];

for (let i = 0; i < numbers.length; i++) {
  if (max < numbers[i]) {
    max = numbers[i];
  }
}

console.log("nilai max", max);
