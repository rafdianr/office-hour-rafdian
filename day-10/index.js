/*
Conditional javascript (if, if else, switch)

Syntax if
if (condition) {
    perintah
} else if ( condition2) {
    perintah
} else {
    perintah
}

switch(parameter) {
    case "case1":
        perintah
        break;
    case "case2":
        perintah
        break;
    default:
        perintah
}
*/

let num = 10

if(num % 2 === 0) {
    // console.log(`${num} adalah genap`)
} else {
    // console.log(`${num} adalah ganjil`)
}

let light = "Merah"

switch(light) {
    case "Merah":
        console.log("stop")
        break;
    case "Kuning":
        console.log("careful")
        break;
    default:
        console.log("drive")
}

/*
Looping (for, while, do while)

for(statement1; statement2; statement3){
    perintah
}

while(statement) {
    perintah
}

do{
    perintah
}
while(statement)
*/

let countWhile = 0
// while(countWhile <= 10) {
//     console.log(countWhile)
//     countWhile++
// }

do {
// console.log(countWhile)
countWhile++
}
while(countWhile<=10)

let books = [ "harpot", "one piece", "naruto"]
for(let i = 0; i < books.length; i++) {
    console.log(books[i])
}

