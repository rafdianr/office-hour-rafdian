# Components, State & Props

## Components

- Komponen adalah bagian-bagian yang menyusun aplikasi React
- Komponen di React bersifat reusable, artinya bisa digunakan kembali
- Ada 2 macam komponen yang sering digunakan, yaitu class
  component dan function component
- Class component juga biasa disebut stateful component
- Function component juga biasa disebut stateless component

### Class Components

- Disebut juga Stateful Component
- Dapat menggunakan React Lifecycle
- Dapat menggunakan state & props

```js
class Latihan extends React.Component {
  render() {
    return (
      <div className="App">
        <h1>Latihan React!</h1>
        <h2>Menggunakan Class component</h2>
      </div>
    );
  }
}
```

```js
import React from "react";

class Latihan extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "My Name",
    };
  }
  render() {
    return (
      <div className="App">
        <h1>Latihan React! oleh {this.state.name}</h1>
        <h2>Menggunakan Class component</h2>
      </div>
    );
  }
}

export default Latihan;
```

### Function Components

- Disebut juga Stateless Component
- Tidak dapat menggunakan React Lifecycle
- Hanya dapat menggunakan props

```js
import React from "react";

const Latihan = (props) => {
  return (
    <div className="App">
      <h1>Latihan React! oleh {this.state.name}</h1>
      <h2>Menggunakan Class component</h2>
    </div>
  );
};

export default Latihan;
```

## State - Props

- State dan props adalah objek khusus yang menyimpan data untuk
  komponen.
- State adalah objek yang digunakan untuk menyimpan data di dalam
  komponen.
- Props adalah objek yang digunakan untuk menyimpan data yang
  diterima dari komponen lainnya.
- Data yang disimpan di dalam state bisa di ubah-ubah.
- Data yang disimpan di dalam props tidak bisa diubah.

### Membuat state

```js
import React from "react";

class Latihan extends React.Component {
  constructor() {
    super();
    this.state = {
      title: "State buat judul",
      subtitle: "State buat subtitle",
    };
  }
  render() {
    return (
      <div className="App">
        <h1>{this.state.title}</h1>
        <h2>{this.state.subtitle}</h2>
      </div>
    );
  }
}

export default Latihan;
```

### Mengubah state

- Mengubah state menggunakan this.setState().
- Setiap kali state berubah, React akan melakukan update tampilan
  komponen pada bagian yang berubah saja.

```js
import React from "react";

class Latihan extends React.Component {
  constructor() {
    super();
    this.state = {
      title: "State buat judul",
      description: "State buat description",
    };
  }

  changeTitle = () => {
    this.setState({
      title: "Judul berubah",
    });
  };

  render() {
    return (
      <div className="App">
        <h1>{this.state.title}</h1>
        <p>{this.state.description}</p>
        <button onClick={this.changeTitle}>Ubah judul</button>
      </div>
    );
  }
}

export default Latihan;
```

### Mengelola props

```js
import React from "react";

const User = (props) => {
  return (
    <div>
      <h4>{props.name}</h4>
      <p>{props.email}</p>
    </div>
  );
};
```
