# High Order Function

Banyak fungsi2 dari javascipt yang termasuk High Order Function, di antaranya adalah

1. forEach()
2. map()
3. filter()
4. find()

Kita akan menggunakan _array_ dibawah ini untuk contoh

```js
const animals = [
  {
    type: "Cat",
    sound: "Meow!",
    legs: 4,
  },
  {
    type: "Dog",
    sound: "Woof!",
    legs: 4,
  },
  {
    type: "Duck",
    sound: "Quack!",
    legs: 2,
  },
];
```

## forEach()

`forEach()` merupakan fungsi dari javascript yang dapat kamu gunakan jika ingin melakukan perulangan terhadap suatu item dalam _array_ yang dilakukan sekali saja. Dengan demikian kita tidak dapat _membuat array_ baru dengan menggunakan nilai yang ada dalam _array_ lain menggunakan `forEach()` .

Dalam cara tradisional kita dapat melakukannya seperti ini:

```js
for (let i = 0; i < animals.length; i++) {
  console.log(animals[i].sound);
}
```

Dengan menggunakan `forEach()` kita dapat melakukannya seperti ini:

```js
animals.forEach((item) => {
  console.log(item.sound);
});
```

## map()

`map()` biasa digunakan jika kita ingin membentuk sebuah _array_ baru dari nilai suatu _array_ lainnya.

Dalam cara tradisional kita dapat melakukannya seperti ini:

```js
let newAnimals = [];

for (let i = 0; i < animals.length; i++) {
  newAnimals.push(animals[i].type);
}

console.log(newAnimals);
```

Dengan menggunakan `map()` kita dapat melakukannya seperti ini:

```js
let newAnimals = animals.map((item) => item.type);

console.log(newAnimals);
```

## filter()

`filter` biasa digunakan jika kita ingin mengambil nilai-nilai dalam sebuah _array_ dan membentuk _array_ baru.

Dalam cara tradisional kita dapat melakukannya seperti ini:

```js
let newAnimals = [];

for (let i = 0; i < animals.length; i++) {
  if (animals[i].legs > 2) {
    newAnimals.push(animals[i]);
  }
}

console.log(newAnimals);
```

Dengan menggunakan `filter()` kita dapat melakukannya seperti ini:

```js
let newAnimals = animals.filter((item) => item.legs > 2);

console.log(newAnimals);
```

## find()

`find()` biasa digunakan jika kita ingin mengambil item dari suatu _array_ yang memiliki spesifikasi tertentu.

Dalam cara tradisional kita dapat melakukannya seperti ini:

```js
const isDog = () => {
  for (let i = 0; i < animals.length; i++) {
    if (animals[i].sound === "Woof!") {
      return animals[i];
    }
  }
};

console.log(isDog());
```

Dengan menggunakan `find()` kita dapat melakukannya seperti ini:

```js
const isDog = animals.find((item) => item.type === "Dog");

console.log(isDog);
```

## Source

[Source](https://github.com/delarta/ga-extra-course)
