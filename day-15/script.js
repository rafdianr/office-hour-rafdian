const animals = [
  {
    type: "Cat",
    sound: "Meow!",
    legs: 4,
  },
  {
    type: "Dog",
    sound: "Woof!",
    legs: 4,
  },
  {
    type: "Duck",
    sound: "Quack!",
    legs: 2,
  },
];

// forEach()
animals.forEach((item) => console.log("forEach()", item.sound));

// let animalsType = animals.forEach((item) => item.type);
// console.log("animalsType", animalsType); // undefined

// for (let i = 0; i < animals.length; i++) {
//   console.log(animals[i].type);
// }

// map()
let animalSounds = animals.map((animal) => animal.sound);

// let animalSounds = [];
// for (let i = 0; i < animals.length; i++) {
//   animalSounds.push(animals[i].sound);
// }

// console.log("map()", animalSounds);

// filter()
let fourLegsAnimals = animals.filter((animal) => animal.legs === 4);
let twoLegsAnimals = animals.filter((animal) => animal.legs === 2);

// let fourLegsAnimals = [];
// for (let i = 0; i < animals.length; i++) {
//   if (animals[i].legs === 4) {
//     fourLegsAnimals.push(animals[i]);
//   }
// }

console.log("filter()", fourLegsAnimals);
console.log("filter()", twoLegsAnimals);

// find()
let isMeow = animals.find((animal) => animal.legs === 4);

// const isDog = () => {
//   for (let i = 0; i < animals.length; i++) {
//     if (animals[i].legs === 4) {
//       return animals[i];
//     }
//   }
// };
// console.log(isDog());

console.log("find()", isMeow);
