/**
 * Buatlah sebuah kode yang akan mencari siswa yang memiliki nilai tertinggi dan nilai rata2 siswa.
 *
 * Input: Array di bawah
 * Output:
 * Nilai rata2: xx
 * Siswa tertinggi: Steve, Pepper
 *
 */

const students = [
  { id: 1, name: "Tony", score: 87 },
  { id: 2, name: "Steve", score: 91 },
  { id: 3, name: "Scott", score: 72 },
  { id: 4, name: "Natasha", score: 66 },
  { id: 5, name: "Bruce", score: 77 },
  { id: 6, name: "Carol", score: 82 },
  { id: 7, name: "Pepper", score: 91 },
  { id: 8, name: "Clint", score: 84 },
  { id: 9, name: "Barton", score: 95 },
];

// fungsi mencari score tertinggi
const getHighestScore = () => {
  let max = students[0].score; // variable penampung diisi pake nilai awal array index ke-0
  // looping array
  for (let i = 0; i < students.length; i++) {
    // kondisi jika nilai lebih besar dari variable max, ganti nilai max dengan nilai array saat ini
    if (students[i].score > max) max = students[i].score;
  }
  return max;
};

let highestScore = getHighestScore(); // Menyimpan hasil fungsi ke dalam variable

// Mendapatkan nama students dengan nilai tertinggi
let highestStudents = students
  .filter((item) => item.score === highestScore) // filter array students pakai nilai tertinggi
  .map((item) => item.name); // mendapatkan nama students

console.log("Siswa tertinggi: ", highestStudents);

// Fungsi mencari nilai rata2 students
const getAvgScore = () => {
  let total = 0; // variable penampung diisi nilai awal 0
  // Looping array students
  for (let i = 0; i < students.length; i++) {
    total += students[i].score; // variable penampung dijumlahkan dengan score students ke-i
  }
  return total / students.length; // return hasil total penjumlahan score dengan panjang array
};

let avgScore = getAvgScore(); // Menyimpan hasil fungsi ke dalam variable
console.log("Nilai rata2: ", avgScore);

// Fungsi mencari score dengan id
const getScoreById = (id) => {
  let score = students.find((item) => item.id === id).score; // find() untuk mencari object yang memiliki id yang sama
  return score;
};

console.log(getScoreById(5));
