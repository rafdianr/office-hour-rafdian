import React, { Component } from "react";
import Header from "../component/Header";
import Counter from "../component/Counter";

export class HomePage extends Component {
  constructor() {
    super();
    this.state = {
      title: "Home Page JSX",
      showCounter: false,
    };
  }

  handleCounter = () => {
    this.setState({ showCounter: !this.state.showCounter });
  };

  render() {
    const desc = "Props description dari variable";
    return (
      <div className="homePage" style={{ textAlign: "center" }}>
        <Header
          title={this.state.title}
          desc={desc}
          name="Props name langsung inline"
        >
          <div>Children 1</div>
          <div>Children 2</div>
          <div>Children 3</div>
        </Header>
        <button onClick={this.handleCounter} style={{ margin: "10px 0" }}>
          {this.state.showCounter ? "Remove counter" : "Show counter"}
        </button>
        <hr style={{ margin: "20px 0" }} />
        {this.state.showCounter ? <Counter /> : null}
      </div>
    );
  }
}

export default HomePage;
