import React, { Component } from "react";
import ButtonComp from "../ButtonComp";

export class Counter extends Component {
  constructor() {
    super();
    this.state = {
      counter: 0,
    };
  }

  componentDidMount() {
    alert("Counter mounting");
  }

  componentDidUpdate() {
    document.title = `Update counter = ${this.state.counter}`;
  }

  componentWillUnmount() {
    alert("Counter unmounting");
  }

  onAdd = () => {
    this.setState({
      counter: this.state.counter + 1,
    });
  };

  onSub = () => {
    this.setState({
      counter: this.state.counter - 1,
    });
  };

  render() {
    return (
      <div className="counter">
        <h1>Counter</h1>
        <h2>{this.state.counter}</h2>
        <ButtonComp textBtn="+" onClick={this.onAdd} />
        <ButtonComp textBtn="-" onClick={this.onSub} />
      </div>
    );
  }
}

export default Counter;
