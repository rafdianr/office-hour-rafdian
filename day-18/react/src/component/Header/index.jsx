import React from "react";

const Header = ({ title, desc, name, children }) => {
  return (
    <div className="header">
      <h1>{title}</h1>
      <p>{desc}</p>
      <p>{name}</p>
      {children}
    </div>
  );
};

export default Header;
