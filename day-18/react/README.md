# Lifecycle

```bash
# using yarn
$ yarn install
$ yarn start

# using npm
$ npm install
$ npm run start
```
