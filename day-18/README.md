# Lifecycle

## Mounting

Method-method pada fase Mounting ini akan dieksekusi saat komponen
sedang dibuat dan akan dimasukkan ke dalam DOM. Dengan kata lain,
fase ini adalah fase ketika pertama kali me-render sebuah komponen.

1. constructor()
2. render()
3. componentDidMount()

## Updating

Method-method pada fase Updating ini akan dieksekusi saat ada
perubahan pada state atau props. Dengan kata lain, fase ini adalah fase
re-render sebuah komponen.

1. render()
2. componentDidUpdate()

## Unmounting

Method pada fase Unmounting ini akan dieksekusi saat komponen
di-remove/dihilangkan dari DOM.

1. componentWillUnmount()
