import React from "react";
import "../assets/styles/button.css";

const Button = ({ textBtn, onClick }) => {
  // const handleClick = () => {
  //   onClick();
  // };
  return (
    <button className="btn btn__round" onClick={() => onClick()}>
      {textBtn}
    </button>
  );
};

export default Button;
