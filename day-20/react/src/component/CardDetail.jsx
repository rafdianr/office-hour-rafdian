import React from "react";
import { useParams } from "react-router-dom";

const CardDetail = ({ data }) => {
  const { slug } = useParams();
  const filteredData = data.find((item) => item.id === parseInt(slug));
  console.log(slug);
  return (
    <div style={{ textAlign: "center" }}>
      <h2>You just selected photos with title: {slug}</h2>
      <p>Title: {filteredData.title}</p>
      <p>URL: {filteredData.url}</p>
    </div>
  );
};

export default CardDetail;
