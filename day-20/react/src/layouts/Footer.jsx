import React from "react";
import "../assets/styles/footer.css";

const Footer = () => {
  return <div className="footer">©2021 office-hour</div>;
};

export default Footer;
