import React from "react";
import "../assets/styles/nav.css";
import { Link, useHistory } from "react-router-dom";

const Header = () => {
  const history = useHistory();

  const goToLocation = () => {
    history.push("/location");
  };

  return (
    <div className="header">
      <div className="nav">
        <Link to="/" className="nav__item">
          Homepage
        </Link>
        <div
          className="nav__item"
          onClick={goToLocation}
          style={{ cursor: "pointer" }}
        >
          New page
        </div>
      </div>
      <hr />
    </div>
  );
};

export default Header;
