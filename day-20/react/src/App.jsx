import "./App.css";
import HomePage from "./page/HomePage";
import LocationPage from "./page/LocationPage";
import Header from "./layouts/Header";
import Footer from "./layouts/Footer";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div>
        <Header />

        <Switch>
          <Route path="/" exact>
            <HomePage />
          </Route>
          <Route path="/location">
            <LocationPage />
          </Route>
          <Route path="*">
            <Redirect to="/" />
          </Route>
        </Switch>

        <Footer />
      </div>
    </Router>
  );
}

export default App;
