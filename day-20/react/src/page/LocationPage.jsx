import React, { useState, useEffect } from "react";
import { useHistory, useRouteMatch, Switch, Route } from "react-router-dom";
import "../assets/styles/location.css";
import Card from "../component/Card";
import CardDetail from "../component/CardDetail";

const Location = () => {
  const [photos, setPhotos] = useState([]);
  const { path, url } = useRouteMatch();
  const history = useHistory();

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/albums/1/photos")
      .then((response) => response.json())
      .then((json) => setPhotos(json));
  }, []);

  const handleClick = (id) => {
    history.push(`${url}/${id}`);
  };

  return (
    <div className="page">
      <div className="location">
        <div className="location__wrapper">
          <div className="location__content">
            <Switch>
              <Route exact path={path}>
                <h2 style={{ textAlign: "center" }}>Select your card!</h2>
                <div className="location__cards">
                  {photos.map((item) => (
                    <Card
                      key={item.id}
                      logo={item.thumbnailUrl}
                      rating={5}
                      title={item.title}
                      description={item.title}
                      onClick={() => handleClick(item.id)}
                    />
                  ))}
                </div>
              </Route>
              <Route path={`${path}/:slug`}>
                <CardDetail data={photos} />
              </Route>
            </Switch>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Location;
