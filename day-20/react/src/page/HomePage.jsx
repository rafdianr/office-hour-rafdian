import React, { Component } from "react";
import Counter from "../component/Counter";

export class HomePage extends Component {
  constructor() {
    super();
    this.state = {
      showCounter: false,
    };
  }

  handleCounter = () => {
    this.setState({ showCounter: !this.state.showCounter });
  };

  render() {
    return (
      <div
        className="homePage"
        style={{ textAlign: "center", height: "100vh" }}
      >
        <h1>Homepage</h1>
        <button onClick={this.handleCounter} style={{ margin: "10px 0" }}>
          {this.state.showCounter ? "Remove counter" : "Show counter"}
        </button>
        {this.state.showCounter ? <Counter /> : null}
      </div>
    );
  }
}

export default HomePage;
