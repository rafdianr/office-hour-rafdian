# Day-4

## Practice

[Button Component](https://www.figma.com/file/L6ocvrKT1XO7yJMk9h7A7y/Office-Hour?node-id=52%3A2)

## Keyframe

Syntax:

```css
@keyframes slidein {
  from {
    transform: translateX(0%);
  }

  to {
    transform: translateX(100%);
  }
}
```
